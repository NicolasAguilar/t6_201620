package taller.mundo;

public class Ciudadano {

	private String nombre;
	private String apellido;
	private String localizacion;
	private int numeroAcudiente;
	
	public Ciudadano(String nom, String ape, String loc, int numAcu)
	{
		nombre = nom;
		apellido = ape;
		localizacion = loc;
		numeroAcudiente = numAcu;
	}
	public String darNombre()
	{
		return nombre;
	}
	
	public String darApellido()
	{
		return apellido;
	}
	
	public String darLocalizacion()
	{
		return localizacion;
	}
	
	public int darNumAcudiente()
	{
		return numeroAcudiente;
	}
	
	
}
