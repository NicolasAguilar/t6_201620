package taller.estructuras;

import java.util.ArrayList;

public class TablaLlaveNoUnica <K,V>
{
	private ArrayList<NodoHash<K, ArrayList<V>>> nodos;

	public TablaLlaveNoUnica()
	{
		nodos = new ArrayList<NodoHash<K, ArrayList<V>>>();
	}

	public void put(K llave, V value)
	{
		boolean agrego = false;
		for(int i=0;i<nodos.size()&&!agrego;i++)
		{
			if(nodos.get(i).getLlave().equals(llave))
			{
				nodos.get(i).getValor().add(value);agrego=true;
			}
		}
		if(!agrego)
		{
			ArrayList<V> a = new ArrayList<>();
			a.add(value);
			NodoHash<K, ArrayList<V>> v = new NodoHash<K,ArrayList<V>>(llave, a);
			nodos.add(v);
		}
	}

	public ArrayList<V> get(K llave)
	{
		ArrayList<V> d = null;
		for(int i=0;i<nodos.size();i++)
		{
			if(nodos.get(i).getLlave().equals(llave) && nodos.get(i)!=null)
				d = nodos.get(i).getValor();
		}
		return d;
	}

	public void delete(K llave)
	{
		for(int i=0;i<nodos.size();i++)
			if(nodos.get(i).getLlave().equals(llave))
				nodos.set(i, null);
		ArrayList<NodoHash<K, ArrayList<V>>> nueva = new ArrayList<NodoHash<K, ArrayList<V>>>();
		for(int i = 0; i<nodos.size();i++)
		{
			if(nodos.get(i)!=null)
				nueva.add(nodos.get(i));
		}
		nodos = nueva;
	}

}
