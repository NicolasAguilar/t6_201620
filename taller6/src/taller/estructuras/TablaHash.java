package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	public NodoHash<K, V>[] nodos;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int n;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int m = 50;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		nodos = new NodoHash[m];
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		m = capacidad;
		nodos = new NodoHash[capacidad];
		this.factorCargaMax = factorCargaMax;
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		if (n >= m/2) resize(2*m);
		int i;
		for(i = hash(llave);nodos[i]!=null;i=(i+1)%m)
			if(nodos[i].getLlave().equals(llave))
				nodos[i].setValor(valor);
		nodos[i] = new NodoHash<K, V>(llave, valor);
		n++;
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		for(int i = hash(llave);nodos[i]!=null;i=(i+1)%m)
			if(nodos[i].getLlave().equals(llave))
				return nodos[i].getValor();
		return null;
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V esper = null;
		int i = hash(llave);
		while (!llave.equals(nodos[i].getLlave()))
			i = (i + 1) % m;
		esper = nodos[i].getValor();
		nodos[i] = null;

		i = (i + 1) % m;
		while (nodos[i] != null)
		{
			K keyToRedo = nodos[i].getLlave();
			V valToRedo = nodos[i].getValor();
			nodos[i] = null;
			n--;
			put(keyToRedo, valToRedo);
			i = (i + 1) % m;
		}
		n--;
		if (n > 0 && n == m/8) resize(m/2);
		return esper;
	}
	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		if(llave instanceof Integer)
			return ((int)llave*3)%m;
		else return 0;
	}

	//TODO: Permita que la tabla sea dinamica
	private void resize(int j)
	{
		TablaHash<K, V> t;
		t = new TablaHash<K,V>(j,0);
		for (int i = 0; i < m; i++)
			if (nodos[i] != null)
				t.put(nodos[i].getLlave(), nodos[i].getValor());
		nodos = t.nodos;
		m = t.m;		
	}
}