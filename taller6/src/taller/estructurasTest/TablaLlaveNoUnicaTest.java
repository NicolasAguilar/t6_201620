package taller.estructurasTest;

import java.util.ArrayList;

import taller.estructuras.NodoHash;
import taller.estructuras.TablaLlaveNoUnica;
import junit.framework.TestCase;

public class TablaLlaveNoUnicaTest extends TestCase{

	TablaLlaveNoUnica<String, Integer> t;

	public void setup()
	{
		t = new TablaLlaveNoUnica<String, Integer>();
		t.put("a", 0);
		t.put("a", 1);
		t.put("a", 2);
		t.put("b", 0);
		t.put("b", 1);
		t.put("b", 2);
	}

	public void testPutGet()
	{
		setup();
		assertEquals(0, (int)t.get("a").get(0));
		assertEquals(1, (int)t.get("a").get(1));
		assertEquals(2, (int)t.get("a").get(2));

		assertEquals(0, (int)t.get("b").get(0));
		assertEquals(1, (int)t.get("b").get(1));
		assertEquals(2, (int)t.get("b").get(2));
	}

	public void testDelete()
	{
		setup();
		t.delete("a");
		assertNull(t.get("a"));
		t.put("c", 5);
		assertEquals(5, (int)t.get("c").get(0));
		assertNotNull(t.get("c"));
		t.delete("c");
		assertNull(t.get("c"));
	}

}
