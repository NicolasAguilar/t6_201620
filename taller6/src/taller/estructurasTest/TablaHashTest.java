package taller.estructurasTest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import taller.estructuras.TablaHash;
import taller.mundo.Ciudadano;
import junit.framework.TestCase;

public class TablaHashTest extends TestCase{

	private static String rutaEntrada = "./data/ciudadLondres.csv";

	TablaHash<Integer, Ciudadano> t;
	public void setup()
	{
		try
		{
			t = new TablaHash<Integer, Ciudadano>();
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				String loc = "0,0";
				t.put(Integer.parseInt(datos[2]), new Ciudadano(datos[0], datos[1], loc, Integer.parseInt(datos[2])));
				entrada = br.readLine();
			}
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void testPutGet()
	{
		setup();
		Ciudadano p = new Ciudadano("0", "0", "0,0", 0);
		t.put(0, p);
		assertEquals(p, t.get(0));
	}
	
	public void testDelete()
	{
		setup();
		Ciudadano p = new Ciudadano("0", "0", "0,0", 0);
		t.put(0, p);
		t.delete(0);
		assertNull(t.get(0));
	}

}
