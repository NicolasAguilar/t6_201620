package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import taller.estructuras.NodoHash;
import taller.estructuras.TablaHash;
import taller.estructuras.TablaLlaveNoUnica;
import taller.mundo.Ciudadano;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		TablaHash<Integer, Ciudadano> t = new TablaHash<>();
		TablaLlaveNoUnica<String, Ciudadano> apell = new TablaLlaveNoUnica<>();
		TablaLlaveNoUnica<String, Ciudadano> localizacionTabla = new TablaLlaveNoUnica<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();
			//TODO: Inicialice el directorio t
			int i = 0;
			int cont = 0;
			while (entrada != null){
				String[] datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				String loc = "("+cont%90+","+(cont*3)%180+")";
				Ciudadano c = new Ciudadano(datos[0], datos[1], loc, Integer.parseInt(datos[2]));
				t.put(Integer.parseInt(datos[2]), c);
				//R3
				apell.put(datos[1], c);
				//R4
				localizacionTabla.put(loc, c);
				++i;
				++cont;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Nombre ciudadano:");
					String nom = br.readLine();
					System.out.println("Apellido ciudadano:");
					String ape = br.readLine();
					System.out.println("Numero acudiente del ciudadano:");
					int n = Integer.parseInt(br.readLine());
					System.out.println("Latitud ciudadano:");
					String la = br.readLine();
					System.out.println("Longitud ciudadano:");
					String lo = br.readLine();
					t.put(n, new Ciudadano(nom, ape, "("+la+","+lo+")", n));
					apell.put(ape, new Ciudadano(nom, ape, "("+la+","+lo+")", n));
					localizacionTabla.put("("+la+","+lo+")", new Ciudadano(nom, ape, "("+la+","+lo+")", n));
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					try{
					System.out.println("Número acudiente del ciudadano:");
					int llave = Integer.parseInt(br.readLine());
					Ciudadano c = t.get(llave);
					if(c!=null)
					{
						System.out.println("Nombre ciudadano: "+c.darNombre());
						System.out.println("Apellido ciudadano: "+ c.darApellido());
						System.out.println("Numero acudiente del ciudadano: " + c.darNumAcudiente());
						System.out.println("Localizacion ciudadano(Latitud,Longitud): "+ c.darLocalizacion());	
					}
					break;
					}
					catch (Exception e2) {
						System.out.println("Telefono no registrado");
						break;
					}
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					try{
					System.out.println("Apellido ciudadano:");
					String apel = br.readLine();	
					ArrayList<Ciudadano> a = apell.get(apel);
					for (Ciudadano ciudadano : a) {
						System.out.println();
						System.out.println("Nombre ciudadano: "+ciudadano.darNombre());
						System.out.println("Apellido ciudadano: "+ ciudadano.darApellido());
						System.out.println("Numero acudiente del ciudadano: " + ciudadano.darNumAcudiente());
						System.out.println("Localizacion ciudadano(Latitud,Longitud): "+ ciudadano.darLocalizacion());				
						System.out.println();
					}
					break;
					}
					catch (Exception e) {
						System.out.println("Apellido no registrado");
						break;
					}
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					try
					{
						System.out.println("Localizacion ciudadano(latitud,longitud):");
						String local = br.readLine();	
						ArrayList<Ciudadano> Clocal = localizacionTabla.get(local);
						for (Ciudadano ciudadano : Clocal) {
							System.out.println();
							System.out.println("Nombre ciudadano: "+ciudadano.darNombre());
							System.out.println("Apellido ciudadano: "+ ciudadano.darApellido());
							System.out.println("Numero acudiente del ciudadano: " + ciudadano.darNumAcudiente());
							System.out.println("Localizacion ciudadano(Latitud,Longitud): "+ ciudadano.darLocalizacion());				
							System.out.println();
						}
					}
					catch (Exception e) {
						System.out.println("Coordenadas invalidas");
						break;
					}
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
