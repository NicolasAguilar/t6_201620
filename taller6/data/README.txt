-�Qu� pasa si llega nueva informaci�n para una llave que ya existe en la tabla de hash?
Se sobreescribe el valor asignado a la llave ya existente.

-�Cu�l es el criterio de crecimiento de la tabla?
Si el numero de llaves actuales supera la mitad de la capacidad actual de la tabla.

-�Cu�l funci�n de hashing escogi� y por qu�?
Llave mod m, es efectiva repartiendo las llaves de manera uniforme desde 0 hasta m-1.

8.
	�Qu� opina de estos resultados? �Si agrega m�s datos los tiempos de ejecuci�n cambian significativamente?
No cambian significativamente ya que no se recorre toda la tabla, gracias a que se conoce la zona por donde esta el dato deseado.
La complejidad del tiempo de acceso en las tablas hash es casi constante, dependiendo de la funcion de hash. 

PARTE 2
-�Tiene sentido hacer una tabla de listas?
Si, ya que en este caso necesitamos conocer todos los valores que se asocian a una llave. Tiene sentido cuando el valor
asociado es mas de uno.

Describa en qu� situaciones las tablas de Hashing no son una buena opci�n. Responda y justifique en el archivo README.txt

No son buena opcion cuando debemos asociar varios valores distintos a una misma llave, en una tabla de hash el valor se
sobreescribe, en esos casos es mejor utilizar una estructura la cual permita asociar varios valores a una llave.